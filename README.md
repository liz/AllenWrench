# Drone Input Tool

Allen Wrench is a desktop tool to aid [Hexcorp](https://hexcorp.net) Drones in their digital interactions.

## WIP Features

### Crucial Updates

* [ ] Ensure window appears within bounds of screen(s) when moved to cursor location
* [ ] Keyboard control of the program
* [X] Auto-send `return` on paste completion for pre-determined phrases
* [ ] Prevent showing window until on correct location

### Nice to Haves

* [ ] Customize shortcut keys used on linux platforms
* [ ] Add support for BSD 😬
* [ ] Improve UI design (Input Requested)
* [ ] Better sorting of subjects
* [ ] Built-in system shortcut
* [ ] Ability to add custom types/phrases
* [ ] [Unicode "font" options](https://lingojam.com/DiscordFonts)
* [ ] Drone Speech Optimizations
* [ ] Improve Launch Times
* [ ] Add Version Display in Config

### Misc notes

[Use this as a guide for building for MacOS](https://thinkgo.io/post/2023/02/publish_tauri_to_apples_app_store/)
