// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use tauri::{Manager, Size, LogicalSize};
use enigo::{Enigo, Key, KeyboardControllable, MouseControllable};
use std::thread;
use std::time::Duration;
use arboard::Clipboard;

#[tauri::command]
fn get_mouse_pos() -> String {
    let enigo = Enigo::new();
    let (x, y) = enigo.mouse_location();
    format!("{{ \"x\": {}, \"y\": {} }}", x, y).into()
}

#[tauri::command]
fn type_str(input: String, autoSend: bool) {
    #[cfg(dev)]
    println!(">: {}", input);

    let mut enigo = Enigo::new();
    let mut clipboard = Clipboard::new().unwrap();

    // Load input into clipboard
    clipboard.set_text(input).unwrap();

    // TODO: Set the Command/Alt key configured by user
    #[cfg(target_os = "macos")] {
        enigo.key_down(Key::Meta);
        enigo.key_click(Key::Tab);
        enigo.key_up(Key::Meta);
    }

    #[cfg(target_os = "windows")] {
        enigo.key_down(Key::Alt);
        enigo.key_click(Key::Tab);
        enigo.key_up(Key::Alt);
    }

    #[cfg(target_os = "linux")] {
        enigo.key_down(Key::Alt);
        enigo.key_click(Key::Tab);
        enigo.key_up(Key::Alt);
    }

    thread::sleep(Duration::from_millis(200));

    #[cfg(target_os = "windows")]{
        enigo.key_down(Key::Control);
        enigo.key_click(Key::Layout('v'));
        enigo.key_up(Key::Control);
    }

    #[cfg(target_os = "macos")]{
        enigo.key_down(Key::Meta);
        enigo.key_click(Key::Layout('v'));
        enigo.key_up(Key::Meta);
    }

    #[cfg(target_os = "linux")]{
        enigo.key_down(Key::Control);
        enigo.key_click(Key::Layout('v'));
        enigo.key_up(Key::Control);
    }

    thread::sleep(Duration::from_millis(200));

    if (autoSend) {
        enigo.key_click(Key::Return);
    }

    thread::sleep(Duration::from_millis(20));

    // Close program
    #[cfg(not(dev))]
    std::process::exit(0);

}

fn main() {
    tauri::Builder::default()
        .setup(|app| {
            let main_window = app.get_window("main").unwrap();

            #[cfg(target_os = "macos")] {
                let _ = main_window.set_size(Size::Logical(LogicalSize {width: 400.0, height: 352.0}));
            }
            
            Ok(())
        })
        .plugin(tauri_plugin_positioner::init())
        .invoke_handler(tauri::generate_handler![type_str, get_mouse_pos])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
    
}
